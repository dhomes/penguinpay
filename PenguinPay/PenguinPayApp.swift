//
//  PenguinPayApp.swift
//  PenguinPay
//
//  Created by dhomes on 10/16/21.
//

import SwiftUI

@main
struct PenguinPayApp: App {
    var body: some Scene {
         
        return WindowGroup {
            NavigationView {
                TransferView()
            }.navigationViewStyle(.stack)
            
        }
    }
}
 
