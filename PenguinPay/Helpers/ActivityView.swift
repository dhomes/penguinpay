//
//  ActivityView.swift
//  PenguinPay
//
//  Created by dhomes on 10/17/21.
//

import SwiftUI
import ActivityIndicatorView

struct ActivityView: View {
    @Binding var isVisible : Bool
    var body: some View {
        VStack {
            Spacer()
            ActivityIndicatorView(isVisible: $isVisible, type: .gradient([.white, .blue]))
                .frame(width: 180, height: 180).padding(50)
                .background(.white.opacity(0.9)).cornerRadius(10).shadow(radius: 3)
                .foregroundColor(.blue)
            Spacer()
        }
    }
}

struct ActivityView_Previews: PreviewProvider {
    static var previews: some View {
        ActivityView(isVisible: .constant(true))
    }
}
