//
//  BinaryOnlyModifier.swift
//  PenguinPay
//
//  Created by dhomes on 10/16/21.
//  - from: https://stackoverflow.com/questions/58733003/swiftui-how-to-create-textfield-that-only-accepts-numbers

import Foundation
import Combine
import SwiftUI

public struct BinaryOnlyViewModifier: ViewModifier {

    @Binding var text: String
    private let maxDigits : Int
    
    public init(text: Binding<String>, maxDigits : Int = 20) {
        self._text = text
        self.maxDigits = maxDigits
    }

    public func body(content: Content) -> some View {
        content
            .keyboardType(.numberPad)
            .onReceive(Just(text)) { newValue in
                let filtered = newValue.filter { "01".contains($0) }
                if filtered != newValue {
                    self.text = String(filtered.prefix(maxDigits))
                } else {
                    if text.count > maxDigits {
                        self.text = String(text.prefix(maxDigits))
                    }
                    
                }
            }
    }
}
