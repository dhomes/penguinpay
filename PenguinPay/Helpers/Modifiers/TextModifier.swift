//
//  TextModifier.swift
//  PenguinPay
//
//  Created by dhomes on 10/16/21.
//

import SwiftUI

struct TextModifier: ViewModifier {
    
    private let textStyle : Font.TextStyle
    private let weight : Font.Weight
    private let color : Color
    private let rounded : Bool
    /// Text modifier with rounded design
    /// - Parameters:
    ///   - style: textStyle, defaults top title3
    ///   - color: textColor, defaults to black
    ///   - weight: weight, defaults to bold
    init(_ style : Font.TextStyle = .title3, color : Color = .black, weight : Font.Weight = .bold, rounded : Bool = true) {
        self.textStyle = style
        self.color = color
        self.weight = weight
        self.rounded = rounded
    }
    
    func body(content: Content) -> some View{
        content.font(Font.system(textStyle, design: rounded ? .rounded : .default).weight(weight)).foregroundColor(color)
    }
}
