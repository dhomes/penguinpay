//
//  AlertX_Ext.swift
//  Asura
//
//  Created by dhomes on 10/13/21.
//

import Foundation
import SwiftUI

extension AlertX {
    static func oneButtonAlert(title : String = "Error",
                               message: String?,
                               buttonText : String = "Continue",
                               action : EmptyClosure? = nil ) -> AlertX {
        
        return AlertX(title: Text(title).bold(),
               message: Text(message ?? "n/a"),
               buttonStack: [.cancel(Text(buttonText), action: { action?() })]
                                                          , theme: .custom(windowColor: .accent.opacity(0.95),
                                                                           alertTextColor: .white,
                                                                           enableShadow: true,
                                                                           enableRoundedCorners: true,
                                                                           enableTransparency: true,
                                                                           cancelButtonColor: .backgroundColor,
                                                                           cancelButtonTextColor: .accent,
                                                                           defaultButtonColor: .white,
                                                                           defaultButtonTextColor: .redColor),
               animation: .fadeEffect())
    }
    
    static func twoButtonAlert(title : String = "Error",
                               message: String?,
                               actionText : String,
                               actionButtonAction : EmptyClosure? = nil,
                               dismissButtonText : String = "Continue",
                               dismissAction : EmptyClosure? = nil) -> AlertX {
        
        return AlertX(title: Text(title).bold(),
               message: Text(message ?? "n/a"),
               buttonStack: [

                             .default(Text(actionText), action: { actionButtonAction?() }),
                             .cancel(Text(dismissButtonText), action: { dismissAction?() }),
                             
                            ]
                      , theme: .custom(windowColor: .blue,
                                                                           alertTextColor: .white,
                                                                           enableShadow: true,
                                                                           enableRoundedCorners: true,
                                                                           enableTransparency: false,
                                                                           cancelButtonColor: .white,
                                                                           cancelButtonTextColor: .red,
                                                                           defaultButtonColor: .white,
                                                                           defaultButtonTextColor: .blue),
               animation: .fadeEffect())
    }
}
