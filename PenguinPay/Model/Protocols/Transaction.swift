//
//  Transaction.swift
//  PenguinPay
//
//  Created by dhomes on 10/17/21.
//

import Foundation

/// Transaction record protocol
protocol Transaction {
    
    /// Transaction Date
    var date : Date { get }
    
    /// Recipient
    var user : User { get }
    
    /// Amount sent, in Binary String
    var sent : String { get }
    
    /// Amount received, in Binary String
    var received : String { get }
    
    /// Transaction id
    var id : String { get }
}
