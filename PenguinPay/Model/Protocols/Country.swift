//
//  Country.swift
//  PenguinPay
//
//  Created by dhomes on 10/16/21.
//

import Foundation

/// Country protocol
protocol Country {
    
    /// Country name
    var countryName : String { get }
    
    /// Phone prefix, includes + symbol
    var phonePrefix : String { get }
    
    /// Number of digits, non - ( ) or spaces, after the phone prefix
    var digitsAfter : Int { get }
    
    /// Country current abbreviation
    var currencyAbbreviation : String { get }
    
    /// Country unique id
    var id : Int { get }
    
    /// Country region
    var region : String { get }
}


