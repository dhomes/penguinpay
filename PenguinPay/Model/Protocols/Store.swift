//
//  Store.swift
//  PenguinPay
//
//  Created by dhomes on 10/17/21.
//

import Foundation

/// Protocol for app-wide state store, right now just Transaction Data but can be expanded so it's shared among view via @EnvironmentObject or @StateObject/@ObservedObject
protocol Store {
    
    /// Transactions array
    var transactions : [Transaction] { get}
    
    /// Add a transaction to the current list
    func addTransaction(_ t : Transaction)
}
