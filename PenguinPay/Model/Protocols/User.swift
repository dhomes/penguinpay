//
//  User.swift
//  PenguinPay
//
//  Created by dhomes on 10/16/21.
//

import Foundation

/// User Protocol
protocol User {
    
    /// First name
    var name : String { get set }
    
    /// Last name
    var lastName : String { get set  }
    
    /// Phone number NOT including country code
    var phoneNumber : String { get set  }
    
    /// Country user resides on
    var country : ServedCountries { get set  }
    
    /// User unique id
    var id : Int { get }
}
