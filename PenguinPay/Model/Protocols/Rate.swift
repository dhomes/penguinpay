//
//  Rate.swift
//  PenguinPay
//
//  Created by dhomes on 10/16/21.
//

import Foundation

/// Simple Rate protocol with CurrencyAbbreviation & current USD to Currency rate
protocol Rate : Codable {
    
    /// Currency Code
    var code : String { get }
    
    /// USD to Currency rate
    var rate : Double { get }
}
