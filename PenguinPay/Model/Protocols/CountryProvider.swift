//
//  CountryProvider.swift
//  PenguinPay
//
//  Created by dhomes on 10/16/21.
//

import Foundation

/// Country provider protocol, set up to return a static enum of country data, but we should look into modifying to a from network source with latest data
/// - ie: Venezuela has changed currency 4 times in the last 10 years
protocol CountryProvider {
    
    /// Current Array of countries that we can send money to
    var countries : [ServedCountries] { get }
}

