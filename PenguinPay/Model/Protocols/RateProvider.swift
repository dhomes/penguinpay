//
//  RateProvider.swift
//  PenguinPay
//
//  Created by dhomes on 10/16/21.
//

import Foundation

/// Protocol for retrieving rates
protocol RateProvider {
    
    /// Get latest Rates
    /// - Returns: array of Rate-conforming ANY
    func getRates() async throws -> [Rate]
}

