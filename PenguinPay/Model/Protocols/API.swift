//
//  API.swift
//  PenguinPay
//
//  Created by dhomes on 10/17/21.
//

import Foundation

/// API / Network protocol, adding here any call directly to App's own backend (vs RateProvider, which comes from a 3rd party service)
protocol API {
    
    /// Sends money to a user, passing binary sending & receiving amounts as String, returns a Transaction
    /// - Returns: Transaction record if successful, throws on error
    func sendMoneyTo(_ user : User, sending : String, receiving : String) async throws -> Transaction
}
