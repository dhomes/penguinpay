//
//  Defines.swift
//  PenguinPay
//
//  Created by dhomes on 10/17/21.
//

import Foundation

/// Basic no-pars completion block
typealias EmptyClosure = () -> ()

/// Simple one-parameter typed completion block
typealias TypedClosure<T> = (T) -> ()
