//
//  TransferModel.swift
//  PenguinPay
//
//  Created by dhomes on 10/16/21.
//

import Foundation
import SwiftUI

/// TransferViewModel, shared among Views
class TransferViewModel : ObservableObject {
    
    /// User receiving the funds
    @Published var destinatary : User? {
        didSet {
            checkStatus()
            guard let destinatary = destinatary else {
                return
            }
            selectedCountry = destinatary.country
        }
    }
    
    /// Country where money is going to
    @Published var selectedCountry : Country {
        didSet {
            // on set, get the latest exchange rate
            setCurrentRate()
            //sending = ""
            computeReceiving()
        }
    }
    
    /// How much we are sending, coverts to receiving as String (MVP: Int value round up)
    @Published var sending : String = "" {
        didSet {
            checkStatus()
            guard let _ = currentRate else {
                return
            }
            computeReceiving()
        }
    }
    
    /// Observer property for confirmation popup
    @Published var transferSuccesful = false
    
    /// Whether the view model has an error in any of its throwables
    @Published var hasError = false
    
    /// The last error received from fetch or send operations
    var error : Error? {
        didSet {
            hasError = error != nil
        }
    }
    
    /// Needed to display recovery option, we can swift to an error enum later
    @Published var errorFetchingRate = false
    
    /// Current (latest for selected country) rate
    @Published var currentRate : Rate?
    
    /// Whether the model is fetching data
    @Published var fetching = false
    
    /// Whether the model is sending data
    @Published var isSending = false

    /// How much the user is receiving
    @Published var receiving : String = "0"
    
    ///  Whether the model has all data needed to transfer the money
    @Published var canTransfer = false
    
    /// private check of conditions
    private func checkStatus() {
        canTransfer = destinatary != nil && sending.count != 0 && sending != "0"
    }
    
    /// Collection of all fetched dates
    private var rates = [Rate]() {
        didSet {
            setCurrentRate()
        }
    }

    /// Error string for View
    var errorString : String { error?.localizedDescription ?? "n/a" }
    
    /// IVars, to be injected through Init, receiving protocols, for Unit testing
    private(set) var countryProvider : CountryProvider
    private(set) var rateProvider : RateProvider
    private(set) var api : API
    
    init(countryProvider : CountryProvider = ServedCountryProvider(),
         rateProvider : RateProvider = CurrentRateProvider(),
         api : API = PenguinAPI()) {
        self.countryProvider = countryProvider
        selectedCountry = countryProvider.countries.first!
        self.rateProvider = rateProvider
        self.api = api
        Task(priority: .background) {
            await fetchLatest()
        }
    }
    
    /// Fetch latest rates
    /**
     - We could implement caching so as to not exceed the API's limit,
     for example, don't re-fetch is the rate was fetched within the last 15 mins,
     but for this simple excersive, will leave as that
    **/
     @MainActor func fetchLatest() async {
        fetching = true
        do {
            async let latestRates = try await rateProvider.getRates()
            rates = try await latestRates
            fetching = false
        } catch {
            errorFetchingRate = true
            fetching = false
        }
    }
    
    /// Executes the transfer view API
    /// - Parameter store: transaction store for adding transaction on success
    @MainActor func sendMoney(store : Store) {
        guard let destinatary = destinatary else {
            return
        }
        isSending = true
        Task(priority: .background) {
            do {
                async let transaction = try await api.sendMoneyTo(destinatary, sending: sending, receiving: receiving)
                store.addTransaction(try await transaction)
                isSending = false
                transferSuccesful = true
            } catch {
                print(error)
                self.error = error
                isSending = false
            }
        }
        
    }
}

// MARK: - PRIVATE FUNCTIONS
private extension TransferViewModel {
    
    /// Sets the current rate from the latest rate & currently selected country
    private func setCurrentRate() {
        currentRate = rates.first(where: {$0.code == selectedCountry.currencyAbbreviation } )
    }
    
    /// Converts a binary number as a string to UInt
    private func binaryToInt(binaryString: String) -> UInt {
      return strtoul(binaryString, nil, 2)
    }
    
    /// Called when new country or rate is set
    private func computeReceiving() {
        guard let rate = currentRate else {
            return
        }
        
        let sending = binaryToInt(binaryString: sending)
        let value = Int(Double(sending) * Double(rate.rate))
        receiving = String(value, radix: 2)
    }
}

// MARK: - STATE STRINGS
extension TransferViewModel {
    /// Simple confirmation String
    var confirmString : String {
        guard let destinatary = destinatary else {
            return ""
        }
        
        return "You are sending USD \(sending) to: \(destinatary.name) \(destinatary.lastName)\n\nProceed?"
    }
    
    /// Simple confirmation string
    var sentString : String {
        guard let destinatary = destinatary else {
            return ""
        }
        return "Your transfer to \(destinatary.componsedName) is on the Way!"
    }
    
    /// Simple string for view
    var exchangeString : String {
        guard let currentRate = currentRate else {
            return "Not available"
        }
        return "1 USD = \(currentRate.rate) \(currentRate.code.uppercased())"
    }
    

}
