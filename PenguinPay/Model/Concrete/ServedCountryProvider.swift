//
//  ServedCountryProvider.swift
//  PenguinPay
//
//  Created by dhomes on 10/16/21.
//

import Foundation

class ServedCountryProvider : CountryProvider, ObservableObject {

    @Published var countries : [ServedCountries] = ServedCountries.allCases
    init() { }
}
