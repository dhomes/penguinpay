//
//  CurrentRateProvider.swift
//  PenguinPay
//
//  Created by dhomes on 10/16/21.
//

import Foundation

/// Using SwiftyJSON for speed vs Codable/Decodable & JSONSerialization
import SwiftyJSON

/// Concrete RateProvider adhering struct
struct CurrentRateProvider : RateProvider {
    
    private let session = URLSession.shared
    private let url = URL(string: "https://openexchangerates.org/api/latest.json?app_id=51dd41bdcaa04ff297c04b682fb53d9b")!
    
    /// Get latest exchange rates
    /// - Returns: array of Rate Protocols
    func getRates() async throws -> [Rate] {
        let (data, _) = try await session.data(from: url)
        let allRates = try JSON(data: data)["rates"].dictionaryValue
        let rates = allRates.map {
            CurrentRate.init(code: $0.key, rate: $0.value.doubleValue)
        }
        return rates
    }
}
