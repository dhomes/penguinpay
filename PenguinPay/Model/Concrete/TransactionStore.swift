//
//  TransactionStore.swift
//  PenguinPay
//
//  Created by dhomes on 10/17/21.
//

import Foundation

/// Observable & Store-conforming app-wide store
class TransactionStore : Store, ObservableObject {
    @Published private(set) var transactions : [Transaction] = []
    func addTransaction(_ t : Transaction) {
        transactions.insert(t, at: 0)
    }
}
