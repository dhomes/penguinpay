//
//  CurrentRate.swift
//  PenguinPay
//
//  Created by dhomes on 10/16/21.
//

import Foundation

/// Concrete Rate-ahering struct
struct CurrentRate : Rate, Codable {
    private(set) var code : String
    private(set) var rate : Double
}

