//
//  PenguinTransaction.swift
//  PenguinPay
//
//  Created by dhomes on 10/17/21.
//

import Foundation

struct PenguinTransaction : Transaction {
    var date : Date
    var user : User
    var sent : String
    var received : String
    var id : String
}

