//
//  Destinatary.swift
//  PenguinPay
//
//  Created by dhomes on 10/16/21.
//

import Foundation

/// Concrete User-conforming ANY receiving the transfer
struct Destinatary : User {
    var name: String
    var lastName: String
    var phoneNumber: String
    var country: ServedCountries
    var id : Int {
        "\(name)\(lastName)\(phoneNumber)".hashValue
    }
}

