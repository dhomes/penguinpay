//
//  Country.swift
//  PenguinPay
//
//  Created by dhomes on 10/16/21.
//

import Foundation

enum ServedCountries : Country, CaseIterable {
    
    case kenya, nigeria, tanzania, uganda
    private var index : Int {
        switch self {
        case .kenya:
            return 0
        case .nigeria:
            return 1
        case .tanzania:
            return 2
        case .uganda:
            return 3
        }
    }
    
    var countryName: String {
        ["kenya","nigeria","tanzania","uganda"][index]
    }
    
    var phonePrefix: String {
        
        ["+254","+234","+255","+256"][index]
    }
    
    var digitsAfter: Int {
        [9,7,9,7][index]
    }
    
    var currencyAbbreviation: String {
        ["KES","NGN","TZS","UGX"][index]
    }
    
    var region : String {
        ["KE","NG","TZ","US"][index]
    }
    
    var id: Int {
        "\(countryName)\(phonePrefix)".hashValue
    }
    
    
    
}


