//
//  PenguinAPI.swift
//  PenguinPay
//
//  Created by dhomes on 10/17/21.
//

import Foundation

/// API conforming struct
struct PenguinAPI : API {
    func sendMoneyTo(_ user : User, sending : String, receiving : String) async throws -> Transaction {
        let transaction : Transaction = try await withCheckedThrowingContinuation { continuation in
             
            Thread.sleep(forTimeInterval: 3)
            let transaction = PenguinTransaction(date: Date(), user: user, sent: sending, received: receiving, id: UUID().uuidString)
                continuation.resume(returning: transaction)
             
        }
        return transaction
    }
}

