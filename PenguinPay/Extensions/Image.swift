//
//  Image.swift
//  PenguinPay
//
//  Created by dhomes on 10/16/21.
//

import Foundation
import SwiftUI

extension Image {
    static func flagFor(_ country : Country) -> Image {
        Image(country.countryName.lowercased())
    }
}
