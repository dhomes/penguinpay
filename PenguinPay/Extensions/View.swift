//
//  Print.swift
//  Merchant
//
//  Created by dhomes on 2/1/21.
//

import Foundation
import SwiftUI
import PopupView

extension View {
    
    func appPopup(isPresented : Binding<Bool>, message : String, isError : Bool) -> some View {
       return self.popup(isPresented: isPresented,
               type: .toast,
               position: .top,
               animation: .easeInOut,
               autohideIn: 3,
               dragToDismiss: true,
               closeOnTap: true,
               closeOnTapOutside: false,
               backgroundColor: Color.black.opacity(0.4),
               dismissCallback: {}, view: {
        
           isError ? AnyView(ErrorPopup(errorString: message)) : AnyView(SuccessPopup(message: message))
        })
        
    }
}


extension View {
    func Print(_ vars: Any...) -> some View {
        for v in vars { print(v) }
        return EmptyView()
    }
    
    
}

extension View {
    typealias ContentTransform<Content: View> = (Self) -> Content
    @ViewBuilder func conditionalModifier<TrueContent: View, FalseContent: View>(
          _ condition: Bool,
          ifTrue: ContentTransform<TrueContent>,
          ifFalse: ContentTransform<FalseContent>) -> some View {
      if condition {
          ifTrue(self)
      } else {
          ifFalse(self)
      }
    }

}

extension View {
    func snapshot() -> UIImage {
        let controller = UIHostingController(rootView: self)
        let view = controller.view

        let targetSize = controller.view.intrinsicContentSize
        view?.bounds = CGRect(origin: .zero, size: targetSize)
        view?.backgroundColor = .clear

        let renderer = UIGraphicsImageRenderer(size: targetSize)

        return renderer.image { _ in
            view?.drawHierarchy(in: controller.view.bounds, afterScreenUpdates: true)
        }
    }
}

#if canImport(UIKit)
extension View {
    func hideKeyboard() {
        UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
    }
}
#endif

