//
//  String.swift
//  PenguinPay
//
//  Created by dhomes on 10/16/21.
//

import Foundation

extension String {
    // FIX for but on iphoneNumberField upon country change
    func fixFor(_ country : ServedCountries) -> String {
        let maxCount = country.digitsAfter
        var fixed : [Character] = []
        let specialChars : [Character] = [" ","-","(",")"]
        var digitsCount = 0
        for (_, char) in self.enumerated() {
            
            if specialChars.contains(char) {
                // do not add to count
                fixed.append(char)
            } else {
                fixed.append(char)
                digitsCount += 1
            }
            if digitsCount == maxCount {
                break
            }
        }
        return String(fixed)
    }
}
