//
//  PenguinTransaction+ext.swift
//  PenguinPay
//
//  Created by dhomes on 10/17/21.
//

import Foundation

extension PenguinTransaction {
    
    /// Simple mock instance, needed for Previews
    static func mock() -> Transaction {
        PenguinTransaction(date: Date(), user: Destinatary(name: "David", lastName: "Homes", phoneNumber: "+555 55 55555", country: .kenya), sent: "11101", received: "11101011", id: UUID().uuidString)
    }
}
