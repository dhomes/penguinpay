//
//  Button.swift
//  PenguinPay
//
//  Created by dhomes on 10/16/21.
//

import Foundation
import SwiftUI

// Helper functions for button creation
extension Button {
    static func button(systemName : String, size : CGFloat, action : @escaping () -> ()) -> Button<AnyView> {
        return Button<AnyView>(action: action, label: {
            AnyView(
                Image(systemName: systemName)
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(width: size, height: size, alignment: .center)
            )
        })
    }
    
    static func closeButton(action : @escaping () -> ()) -> Button<AnyView> {
        return Button<AnyView>(action: action, label: {
            AnyView(
                Image(systemName: "xmark.circle.fill")
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(width: 25, height: 25, alignment: .center)
                    .padding([.bottom,.top], 10).padding(.leading, -5).foregroundColor(Color(UIColor.lightGray))
            )
        })
    }
    
    static func button(leftText : String, systemName : String, size : CGFloat, action : @escaping () -> ()) -> Button<AnyView> {
        return Button<AnyView>(action: action, label: {
            AnyView(
                HStack {
                    Text(leftText).font(.system(.title3, design: .rounded)).fontWeight(.semibold)
                    Image(systemName: systemName)
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .frame(width: size, height: size, alignment: .center)
                }
                
            )
        })
    }
    
    static func button(rightText : String, systemName : String, size : CGFloat, action : @escaping () -> ()) -> Button<AnyView> {
        return Button<AnyView>(action: action, label: {
            AnyView(
                HStack {
                    Image(systemName: systemName)
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .frame(width: size, height: size, alignment: .center)
                    Text(rightText
                    ).font(.system(.title3, design: .rounded)).fontWeight(.semibold)
                }
            )
        })
    }
}


