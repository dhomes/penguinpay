//
//  User+ext.swift
//  PenguinPay
//
//  Created by dhomes on 10/17/21.
//

import Foundation

extension User {
    
    /// Composed (Country Code + phone) phone number
    var composedPhone : String {
        "\(country.phonePrefix) \(phoneNumber)"
    }
    
    /// Composed (first + last names) & capitalized user data
    var componsedName : String {
        "\(name) \(lastName)".capitalized
    }
}
