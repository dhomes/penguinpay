//
//  Destinatary+Extensions.swift
//  PenguinPay
//
//  Created by dhomes on 10/16/21.
//

import Foundation

extension Destinatary {
    /// Simple mock object for preview
    static func mock() -> User {
        Destinatary(name: "David", lastName: "Homes", phoneNumber: "555 555 5555", country: .kenya)
    }
}
