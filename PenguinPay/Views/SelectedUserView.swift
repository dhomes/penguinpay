//
//  SelectedUserView.swift
//  PenguinPay
//
//  Created by dhomes on 10/16/21.
//

import SwiftUI

/// Selected user subview component, used by TransferInputView
struct SelectedUserView: View {
    
    /// User-Adhering type (destinatary)
    let user : User
    var body: some View {
        VStack(alignment: .leading, spacing: 5) {
                
                // MARK: - BASIC USER INFO
            Text("\(user.componsedName)".capitalized)
                .modifier(TextModifier(.body,
                                       weight: .bold,
                                       rounded: false))
                
            Text("tlf: \(user.composedPhone)")
                .modifier(TextModifier(.subheadline,
                                       color: .accent,
                                       weight: .medium))
            }
 
        
    }
}

struct SelectedUserView_Previews: PreviewProvider {
    static let user : User = Destinatary.mock()
    static var previews: some View {
        SelectedUserView(user : user).padding()
    }
}
