//
//  ConfirmPopup.swift
//  PenguinPay
//
//  Created by dhomes on 10/17/21.
//

import SwiftUI

struct SuccessPopup: View {
    let message : String
    var body: some View {
        VStack {
            Spacer().frame(height: 30)
            HStack {
                
                Text(message)
                    .font(.title2)
                    .fontWeight(.medium)
                    .multilineTextAlignment(.leading)
                    .padding(.horizontal)
                    .foregroundColor(.white)
                Spacer()
            }.padding().background(.blue)
                .cornerRadius(10)
                .shadow(radius: 3)
        }
        .padding()
    }
}
