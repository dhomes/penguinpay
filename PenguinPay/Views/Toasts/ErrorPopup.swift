//
//  ErrorPopup.swift
//  PenguinPay
//
//  Created by dhomes on 10/17/21.
//

import SwiftUI

struct ErrorPopup: View {
    let errorString : String
    var body: some View {
        VStack {
            Spacer().frame(height: 30)
            HStack {
                Text(errorString)
                    .font(.title2)
                    .fontWeight(.medium)
                    .multilineTextAlignment(.leading)
                    .padding(.horizontal)
                    .foregroundColor(.white)
                Spacer()
            }.padding()
                .background(.red)
                .cornerRadius(10)
                .shadow(radius: 3)
        }.padding()
    }
}
