//
//  MainInputView.swift
//  PenguinPay
//
//  Created by dhomes on 10/16/21.
//

import SwiftUI

/// View Input transfer (upper screen component above Banner & Trxlist)
struct TransferInputView: View {
    
    /// Observed trx store
    @ObservedObject var store : TransactionStore
    
    /// Observed TransferViewModel
    @ObservedObject var model : TransferViewModel
    
    /// Informs superview that the user selection button was tapped
    @Binding var selectionTapped : Bool
    
    /// State to toggle for user to confirm trx
    @State private var sendTapped = false
    
    var body: some View {
        
        GeometryReader { geo in
            VStack {
                ZStack {
                    // MARK: - SELECTED USER
                    VStack(alignment: .leading, spacing: 5) {
                        Text("Send Money")
                            .modifier(TextModifier(.title3,
                                                   weight: .bold))
                        Text("TO")
                            .modifier(TextModifier(.subheadline,
                                                   color: .darkGray,
                                                   weight: .bold))
                        
                        // MARK: - RECIPIENT BUTTON
                        Button {
                            withAnimation {
                                selectionTapped.toggle()
                            }
                        } label: {

                            ZStack(alignment: .leading) {
                                // Selected user view
                                Unwrap(model.destinatary) { destinatary in
                                    SelectedUserView(user: destinatary)
                                        .padding(.top, 5)
                                }
                                
                                // When no recipient has been selected
                                TextField("Select Recipient",
                                          text: .constant(""),
                                          prompt: Text("Select Recipient"))
                                    .multilineTextAlignment(.leading)
                                        .conditionalModifier(model.destinatary == nil)
                                    { $0 } ifFalse: { $0.hidden() }
                                
                                // Currently selected country flag
                                HStack {
                                    Spacer()
                                    Image.flagFor(model.selectedCountry)
                                        .resizable()
                                        .scaledToFit()
                                        .frame(width: 40, height: 40)
                                }
                            }
                        }
                        Divider()
                    }
                }
                
                VStack(alignment: .leading)  {
                    // MARK: - SEND AMOUNT
                    Group {
                        Text("SEND")
                            .modifier(TextModifier(.subheadline,
                                                   color: .darkGray,
                                                   weight: .bold))
                        
                        ZStack(alignment: .leading) {
                            TextField("Amount",
                                      text: $model.sending,
                                      prompt: Text("Amount (in binary)"))
                                .modifier(BinaryOnlyViewModifier(text: $model.sending))
                                .padding(.vertical, 5)
                                .keyboardType(.numberPad)
                            HStack {
                                Spacer()
                                Text("USD").bold()
                            }
                        }
                        
                        Divider()
                    }
                    
                    Group {
                        // MARK: - SEND RECEIVE AMOUNT
                        Text("RECEIVE")
                            .modifier(TextModifier(.subheadline,
                                                   color: .darkGray,
                                                   weight: .bold))
                        HStack {
                            Text(model.receiving)
                                .multilineTextAlignment(.leading)
                            Spacer()
                            Text(model.selectedCountry.currencyAbbreviation)
                                .bold()
                        }.padding(.top,2.5)
                        Divider()
                    }
                }
                // MARK: - EXCHANCE RATE FOR SELECTED COUNTRY - ADD FAILURE STATE
                Group {
                    
                    if model.fetching {
                        HStack(spacing: 5) {
                            Text("Exchange Rate: ")
                            ProgressView().tint(.gray)
                        }
                    } else {
                        Text("Exchange Rate: \(model.exchangeString)")
                            .multilineTextAlignment(.center)
                            .fixedSize(horizontal: false, vertical: true)
                    }
                }.modifier(TextModifier(.subheadline,
                                        color: .gray,
                                        weight: .bold,
                                        rounded: true))
                
                // MARK: - SEND BUTTON, TRIGGERS CONFIRMATION
                Button {
                    sendTapped.toggle()
                } label: {
                    HStack {
                        Spacer()
                        Text("Send").bold()
                        Image(systemName: "paperplane.fill")
                        Spacer()
                    }
                    .foregroundColor(.white)
                    .padding()
                    .background(
                        Capsule()
                            .fill(model.canTransfer ? Color.blue : Color.blue.opacity(0.25))
                    )
                }.disabled(!model.canTransfer)

            }
            .alertX(isPresented: $sendTapped) {
                AlertX.twoButtonAlert(title: "Confirm Transfer",
                                      message: model.confirmString,
                                      actionText: "Yes, transfer",
                                      actionButtonAction: {
                    model.sendMoney(store: store)
                }, dismissButtonText: "Cancel", dismissAction: {})
            }
            .padding()
            .background(.white.opacity(0.985))
            .cornerRadius(10).overlay(
                RoundedRectangle(cornerRadius: 10)
                    .stroke(Color.darkGray, lineWidth: 0.25)
            )
        }
        
    }
}

struct MainInputView_Previews: PreviewProvider {
    static let model = TransferViewModel()
    static let store = TransactionStore()
    static var previews: some View {
        TransferInputView(store: store, model: model, selectionTapped: .constant(false)).padding()
    }
}
