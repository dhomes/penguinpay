//
//  TransactionViewRow.swift
//  PenguinPay
//
//  Created by dhomes on 10/17/21.
//

import SwiftUI

/// Simple row for each transaction
struct TransactionViewRow: View {

    let transaction : Transaction

    private static let dateFormatter : DateFormatter = {
       let df = DateFormatter()
        df.dateStyle = .medium
       return df
    }()

    private static let timeFormatter : DateFormatter = {
       let df = DateFormatter()
        df.dateStyle = .none
        df.timeStyle = .short
       return df
    }()
    
    var body: some View {
        HStack(alignment: .top) {
            // MARK: - FLAG & COUNTRY NAME
            VStack(spacing: 0) {
                Image.flagFor(transaction.user.country)
                Text(transaction.user.country.countryName.capitalized)
                    .modifier(TextModifier(.subheadline,
                                           color: .white,
                                           weight: .light,
                                           rounded: false))
                    .padding(2)
                    .background(.blue)
                    .cornerRadius(5)
            }
            
            // MARK: - ADDITIONAL TRANSFER DATA PER ROW
            VStack(alignment: .leading, spacing: 2) {
                Text("Sent USD \(transaction.sent) to \(transaction.user.name) \(transaction.user.lastName)")
                    .modifier(TextModifier(.title3,
                                           weight: .medium,
                                           rounded: false))
                Text("Received: \(transaction.received) \(transaction.user.country.currencyAbbreviation)")
                    .modifier(TextModifier(.body,
                                           weight: .regular,
                                           rounded: false))
                HStack {
                    Text("On: \(Self.dateFormatter.string(from: transaction.date))")
                    Text("at \(Self.timeFormatter.string(from: transaction.date))")
                }.modifier(TextModifier(.subheadline,
                                        color: .darkGray,
                                        weight: .medium,
                                        rounded: false))
            }
            Spacer()
        }.padding()
    }
}

struct TransactionViewRow_Previews: PreviewProvider {
    static let transaction = PenguinTransaction.mock()
    static var previews: some View {
        TransactionViewRow(transaction: transaction)
    }
}
