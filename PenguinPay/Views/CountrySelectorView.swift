//
//  CountrySelectorView.swift
//  PenguinPay
//
//  Created by dhomes on 10/16/21.
//

import SwiftUI

/// Country selector view
struct CountrySelectorView: View {
    
    /// Currently selected country (or default if any)
    @Binding var selectedCountry : ServedCountries
    
    /// For dismissal
    @Environment(\.presentationMode) private var presentationMode
    var body: some View {
        NavigationView {
            ScrollView {
                ForEach(ServedCountries.allCases, id: \.id) { country in
                    // MARK: - SELECT COUNTRY AND DISMISS VIEW BUTTON
                    Button {
                        selectedCountry = country
                        presentationMode.wrappedValue.dismiss()
                    } label: {
                        // MARK: - BUTTON VIEW, PER COUNTRY
                        VStack {
                            ZStack(alignment: .leading) {
                                HStack {
                                    Image.flagFor(country)
                                    Text(country.countryName.capitalized)
                                        .foregroundColor(.black)
                                }
                                HStack {
                                    Spacer()
                                    if country == selectedCountry {
                                        Image(systemName: "checkmark")
                                    }
                                }
                            }.padding(.vertical,5)
                                .padding(.horizontal)
                            Divider()
                        }
                    }
                }.navigationTitle("Select Country")
                    .navigationBarItems(leading:
                                            Button<AnyView>.closeButton {
                        presentationMode
                            .wrappedValue
                            .dismiss()
                    })
            }
        }.navigationViewStyle(.stack)
    }
}

struct CountrySelectorView_Previews: PreviewProvider {
    static let selected = ServedCountries.kenya
    static var previews: some View {
        CountrySelectorView(selectedCountry : .constant(selected))
    }
}
