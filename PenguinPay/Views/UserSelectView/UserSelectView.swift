//
//  UserSelectView.swift
//  PenguinPay
//
//  Created by dhomes on 10/16/21.
//

import SwiftUI
import PhoneNumberKit
import iPhoneNumberField

/// User selection view
struct UserSelectView: View {
    // MARK: - BINDINGS & STATE
    @ObservedObject var model : TransferViewModel
    @StateObject private var selectModel : UserSelectViewModel
    @Binding var showUserSelect : Bool
    
    // MARK: - PRIVATE IVARS
    @Environment(\.presentationMode) private var presentationMode
    @FocusState private var focusFirstName : Bool
    @FocusState private var focusLastName : Bool
    @FocusState private var focusPhoneFieldName : Bool
    @State private var showSelector : Bool = false

    // MARK: - INITIALIZER
    init(model : TransferViewModel, showUserSelect : Binding<Bool>) {
        self.model = model
        self._showUserSelect = showUserSelect
        self._selectModel =  StateObject(wrappedValue: UserSelectViewModel(selectedUser: model.destinatary))
    }

    
    var body: some View {
        VStack {
            // MARK: - TOP LABEL & CLOSE
            HStack {
                VStack(alignment: .leading, spacing: 0) {
                    Button<AnyView>.closeButton {
                        showUserSelect.toggle()
                    }
                    Text("Enter User").modifier(TextModifier(.title3, color: .black, weight: .bold, rounded: true))
                }
                Spacer()
            }.padding(.bottom)
            
            
            HStack {
                // MARK: - FIRST NAME
                VStack(alignment: .leading) {
                    Text("First Name")
                        .modifier(TextModifier(.subheadline,
                                               color: .black,
                                               weight: .medium,
                                               rounded: false))
                    
                    TextField("",
                              text: $selectModel.name,
                              prompt: Text("First Name")).onSubmit {
                        focusLastName = true
                    }
                              .submitLabel(.next)
                              .focused($focusFirstName)
                    Divider()
                }
                
                // MARK: - LAST NAME
                VStack(alignment: .leading) {
                    Text("Last Name")
                        .modifier(TextModifier(.subheadline,
                                               color: .black,
                                               weight: .medium,
                                               rounded: false))
                    TextField("",
                              text: $selectModel.lastName,
                              prompt: Text("Last Name")).onSubmit {
                        focusPhoneFieldName = true
                    }.submitLabel(.next)
                        .focused($focusLastName)
                    Divider()
                }
            }
            Spacer().frame(height: 25)
            
            // MARK: - PHONE NUMBER & COUTRY LABELS
            VStack(alignment: .leading) {
                HStack {
                    Text("Phone number")
                        .modifier(TextModifier(.subheadline,
                                               color: .black,
                                               weight: .medium,
                                               rounded: false))
                    Spacer()
                    Text("Country")
                        .modifier(TextModifier(.subheadline,
                                               color: .black,
                                               weight: .medium,
                                               rounded: false)).padding(.horizontal)
                }
                
                // MARK: - PHONE FIELD AND COUNTRY SELECTOR
                ZStack {
                    HStack {
                        Text(selectModel.country.phonePrefix)
                        ZStack(alignment: .leading) {
                            if selectModel.phoneNumber.count == 0 {
                                Text("Phone number").foregroundColor(.gray.opacity(0.5))
                            }
                            
                            
                            iPhoneNumberField("Phone", text: $selectModel.phoneNumber).prefixHidden(true)
                                .defaultRegion(selectModel.country.region)
                                .maximumDigits(selectModel.country.digitsAfter)
                                .formatted(true)
                        }
                    }
                    // MARK: - SET USER BUTTON
                    HStack {
                        Spacer()
                        Button {
                            showSelector = true
                        } label: {
                            VStack(spacing: 0) {
                                Image.flagFor(selectModel.country).padding(.horizontal)
                            }
                        }
                    }
                }
                Divider()
            }
            // MARK: - SET USER BUTTON
            HStack {
                Spacer()
                Button(action: {
                    model.destinatary = selectModel.user()
                    withAnimation {
                        showUserSelect.toggle()
                    }
                }) {
                    HStack {
                        Image(systemName: "person.circle.fill")
                        Text("Set User").fontWeight(.semibold)
                    }
                }
                .padding(7.5)
                .foregroundColor(.white)
                .background(selectModel.canAdd ? .blue : .blue.opacity(0.25))
                .clipShape(RoundedRectangle(cornerRadius: 5))
                .disabled(!selectModel.canAdd)
            }
        }
        .sheet(isPresented: $showSelector) {
            CountrySelectorView(selectedCountry: $selectModel.country)
        }
        .padding(.horizontal)
        .padding(.bottom)
        .background(.white)
        .cornerRadius(10)
        .overlay(
            RoundedRectangle(cornerRadius: 10)
                .stroke(Color.darkGray, lineWidth: 0.5)
        )
        .onAppear {
            selectModel.setSelected(model.destinatary)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.25) {
                focusFirstName = true
            }
        }
    }
}


struct UserSelectView_Previews: PreviewProvider {
    static let model = TransferViewModel()
    static var previews: some View {
        UserSelectView(model: TransferViewModel(), showUserSelect: .constant(false))
    }
}
