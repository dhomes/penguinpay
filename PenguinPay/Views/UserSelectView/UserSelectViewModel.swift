//
//  UserSelectViewModel.swift
//  PenguinPay
//
//  Created by dhomes on 10/16/21.
//

import Foundation
import Combine

/// User SelectViewModel
class UserSelectViewModel : ObservableObject {
    
    /// Input name
    @Published var name : String = "" {
        didSet {
            passthroughObject.send()
        }
    }
    
    /// Input lastName
    @Published var lastName : String = "" {
        didSet {
            passthroughObject.send()
        }
    }
    
    /// Input phoneNumber
    @Published var phoneNumber : String = "" {
        didSet {
            passthroughObject.send()
        }
    }
    
    /// currently selected country
    @Published var country : ServedCountries = .kenya {
        didSet {
            passthroughObject.send()
        }
    }
    
    /// currently selected user
    @Published private(set) var selectedUser : User? {
        didSet {
            canAdd = selectedUser != nil
        }
    }
    
    /// flag for enabling / disabling user
    @Published var canAdd = false
    
    /// Storage for cancellables
    private var cancellables = Set<AnyCancellable>()
    
    /// Passthrough object to receive changes to other properties
    @Published private var passthroughObject : PassthroughSubject<Void, Never>
    
    init(selectedUser : User? = nil) {
        
        passthroughObject = PassthroughSubject<Void,Never>()
        passthroughObject.sink(receiveValue: { [unowned self] _ in
            self.evaluate()
        }).store(in: &cancellables)
        if let selectedUser = selectedUser {
            setSelected(selectedUser)
        }
    }
    
    /// Check whether we have valid data for adding a user && sets selectedUser if so
    private func evaluate() {
        let validName = name.count >= 2
        let validLastName = lastName.count >= 2
        let stripped = phoneNumber
            .replacingOccurrences(of: " ", with: "")
            .replacingOccurrences(of: "-", with: "")
            .prefix(country.digitsAfter)
        canAdd = validName && validLastName && stripped.count == country.digitsAfter
        if canAdd {
            let user = Destinatary(name: name, lastName: lastName, phoneNumber: phoneNumber.fixFor(country), country: country)
            self.selectedUser = user
        }
    }
    
    /// User setter when UserSelectionView already has a user
    func setSelected(_ user : User?) {
        if let user = user {
            name = user.name
            lastName = user.lastName
            phoneNumber = user.phoneNumber
            country = user.country
        }
        selectedUser = user
    }
    
    /// The currently selected user
    func user() -> User? {
        return selectedUser
    }
    
}
