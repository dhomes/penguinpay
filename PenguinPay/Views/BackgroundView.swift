//
//  BackgroundView.swift
//  PenguinPay
//
//  Created by dhomes on 10/16/21.
//

import SwiftUI

// Simple SendWave-like background view
struct BackgroundView: View {
    var body: some View {
        GeometryReader { geo in
            VStack {
                Rectangle()
                    .fill(Color.accent)
                    .frame(height: 250)
                Spacer()
            }.frame(maxWidth: .infinity,
                    maxHeight: .infinity)
            .background(Color.backgroundColor)
            .edgesIgnoringSafeArea([.top,.bottom])
        }
    }
}

struct BackgroundView_Previews: PreviewProvider {
    static var previews: some View {
        BackgroundView()
    }
}
