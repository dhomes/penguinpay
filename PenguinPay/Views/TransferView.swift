//
//  ContentView.swift
//  PenguinPay
//
//  Created by dhomes on 10/16/21.
//

import SwiftUI
import PopupView

/// The main trasfer view (user selector + banner view + transaction list
struct TransferView: View {
    
    // We could move these @StateObject to @Environment as the app grows
    @StateObject var model = TransferViewModel()
    @StateObject var store = TransactionStore()
    
    @State var showUserSelector = false
    var body: some View {
        GeometryReader { geo in
            ZStack {
                VStack {
                    ScrollView(showsIndicators : false) {
                    Text("PenguinPay")
                        .modifier(TextModifier(.title2,
                                               color: .white,
                                               weight: .heavy,
                                               rounded: true))
                    
                        ZStack() {
                            //MARK: - TRANSFER DETAIL VIEW
                            
                            TransferInputView(store: store, model: model,
                                          selectionTapped: $showUserSelector)
                                .padding([.leading,.trailing, .bottom])
                                .blur(radius: showUserSelector ? 3.0 : 0)

                            //MARK: - USER SELECTOR
                            UserSelectView(model: model, showUserSelect: $showUserSelector)
                                .padding()
                                .padding(.horizontal,10).conditionalModifier(showUserSelector) {
                                    $0
                                } ifFalse: {
                                    $0.hidden()
                                }
                            
                            
                        }.padding(.bottom)
                    // MARK: - BANNER & TRANSACTION LIST
                    VStack {
                        Spacer().frame(height: 20)
                        BannerView().padding().frame(height: 220)
                        TransactionsListView(store: store).padding()
                    }.blur(radius: showUserSelector ? 3.0 : 0)
                    
                }
                
                }.frame(width: geo.size.width, height: geo.size.height)
            .blur(radius: model.isSending ? 3.0 : 0)
                ActivityView(isVisible: $model.isSending)
            }
                .navigationBarHidden(true)
                .background(BackgroundView())
                .navigationTitle("")
                .alertX(isPresented: $model.errorFetchingRate) {
                    AlertX.twoButtonAlert(title: "Fetch error", message: "The latest rate could not be retried\n\nTry again?", actionText: "Retry", actionButtonAction: {
                        Task {
                            await model.fetchLatest()
                        }
                    }, dismissButtonText: "Cancel") {}
                }
                .appPopup(isPresented: $model.hasError,
                          message: model.errorString,
                          isError: true)
                .appPopup(isPresented: $model.transferSuccesful,
                          message: model.sentString, isError: false)
        }
        
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            TransferView()
        }.navigationViewStyle(.stack)
        
    }
}




