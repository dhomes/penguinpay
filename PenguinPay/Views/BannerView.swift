//
//  BannerView.swift
//  PenguinPay
//
//  Created by dhomes on 10/17/21.
//

import SwiftUI

/// Static banner view, just to emulate SendWave's app look & feel
struct BannerView: View {
    var body: some View {
        GeometryReader { geo in
            VStack {
                // MARK: - TOP TITLE
                Text("Rapid, *no fee* transfer")
                    .modifier(TextModifier(.title,
                                                                     color: .bannerText,
                                                                     weight: .bold,
                                                                       rounded: false))
                    .multilineTextAlignment(.center)
                    .minimumScaleFactor(0.5)
                ZStack {
                    VStack(alignment: .leading) {
                        // MARK: - TOP SEND TEXT
                        Text("Send to Airtel and MTN mobile wallets & select banks")
                            .modifier(TextModifier(.caption,
                                                   color: .darkGray,
                                                   weight: .medium,
                                                   rounded: false))
                            .multilineTextAlignment(.leading)
                            .fixedSize(horizontal: false, vertical: true)
                            .padding(.top,10)
                            .padding(.horizontal)
                             
                        // MARK: - IMAGES
                        HStack(spacing: 20) {
                            Spacer()
                            Image("airtel")
                                .resizable()
                                .scaledToFit()
                                .frame(width: 60, height: 80)
                            Image("mtn-logo")
                                .resizable()
                                .scaledToFit()
                                .frame(width: 50, height: 50)
                            Image(systemName : "building.columns.fill")
                                .resizable()
                                .scaledToFit()
                                .frame(width: 50, height: 50)
                                .foregroundColor(.accent)
                            Spacer()
                        }.background()
                            .padding(.trailing,50)
                    }.frame(maxWidth: .infinity)
                        .background(.white)
                    
                    // MARK: - MASCOT/PENGUIN IMAGE
                    HStack {
                        Spacer()
                        Image("mascot")
                            .resizable()
                            .scaledToFit()
                            .frame(width: 120, height: 120)
                            .offset(x: 40, y: 40)
                    }
                }
            }.padding()
                .frame(width: geo.size.width)
                .background(Color.bannerBackground)
                .cornerRadius(10)
        }
    }
}

struct BannerView_Previews: PreviewProvider {
    static var previews: some View {
        BannerView().padding()
    }
}
