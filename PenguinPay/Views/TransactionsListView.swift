//
//  TransactionsListView.swift
//  PenguinPay
//
//  Created by dhomes on 10/17/21.
//

import SwiftUI

/// Bottom TransactionListView
struct TransactionsListView: View {
    
    /// Observer transaction store
    @ObservedObject var store : TransactionStore
    
    var body: some View {
        VStack(alignment: .leading) {
            // MARK: - HEADER WHEN .COUNT > 0
            if store.transactions.count > 0 {
                Text("Recent Transactions")
                    .modifier(TextModifier(.body,
                                           color: .darkGray,
                                           weight: .bold,
                                           rounded: false)).padding()
            }
            LazyVStack(alignment: .leading,
                       spacing: 0,
                       pinnedViews: [.sectionHeaders]) {
                // MARK: - NO TRANSACTIONS
                if store.transactions.count == 0 {
                    Text("You have no transactions. To create your first transfer, choose your recipient and enter the amount in the Send Field")
                        .modifier(TextModifier(.body,
                                               color: .darkGray,
                                               weight: .semibold,
                                               rounded: false))
                        .multilineTextAlignment(.center).padding()
                } else {
                    // MARK: - NO ROWS
                    Section() {
                        ForEach(store.transactions,id: \.id) { transaction in
                            VStack(spacing: 0) {
                                TransactionViewRow(transaction: transaction)
                                Divider()
                            }
                        }
                    }
                }
            }.background(.white)
             .cornerRadius(10)
        }
        .padding(.horizontal,2)
    }
}

struct TransactionsListView_Previews: PreviewProvider {
    static let store = TransactionStore()
    static var previews: some View {
        TransactionsListView(store: store)
    }
}
