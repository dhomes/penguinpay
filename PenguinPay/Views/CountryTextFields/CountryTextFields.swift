//
//  CountryTextFields.swift
//  PenguinPay
//
//  Created by dhomes on 10/16/21.
//

import Foundation
import PhoneNumberKit

/// Convenience factory of PhoneNumberTextField as f(country)
struct CountryTextFields {

    private class CountryTextField: PhoneNumberTextField {
        init(country : ServedCountries, numberKit : PhoneNumberKit) {
            super.init(frame: .zero, phoneNumberKit: numberKit)
            defaultRegion = country.region
        }
        
        required init(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        override var defaultRegion: String {
            get {
                return ServedCountries.tanzania.region
            }
            set {}
        }
    }
    
    /// Returns a PhoneNumberTextField subclass
    /// - Parameters:
    ///   - country: country for the text field (sets region)
    ///   - kit: PhoneNumberKit instance
    /// - Returns: a PhoneNumberTextField that accepts phone #s formatted in the country's region
    static func fieldFor(_ country : ServedCountries,
                         kit: PhoneNumberKit) -> PhoneNumberTextField {
        CountryTextField(country: country, numberKit: kit)
    }
}
