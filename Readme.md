**Oct 17, 2021**

---

## PenguinPay

### Developer: David Homes, dhomes@gmail.com

Created for Sendwave as part of selection process.

## MVP Constraints
1. iPhone only.
2. Portrait Orientation.
3. iOS 15.
4. Country data comes from a static enum, should come from a network request in the future (some countries change currencies now and then)
5. Transfers are limited to UInts (i.e: receiving is floor-rounded to Int)
---


## Dev notes
1. SwiftUI 3.
2. await/asynch for networking.
3. Basic combine (@Published & Passthrough objects).
4. Protocol oriented (as much as possible within the test time).


---

## Dependencies

(SPM only, no Cocoapods or Carthage)

1. SwiftyJson (for dev speed vs Codable protocol).
2. PopupView.
3. ActivityIndicatorView.
4. iPhoneNumberField.
5. PhoneNumberKit.
