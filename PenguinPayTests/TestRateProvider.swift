//
//  TestModel.swift
//  PenguinPayTests
//
//  Created by dhomes on 10/17/21.
//

import XCTest
@testable import PenguinPay

class TestModel: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
  
    }

    func testRateProvider() async throws {
        let provider = CurrentRateProvider()
        let rates = try await provider.getRates()
        XCTAssert(rates.count > 0)
        
    }
}
