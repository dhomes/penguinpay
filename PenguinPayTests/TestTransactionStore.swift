//
//  TestTransactionStore.swift
//  PenguinPayTests
//
//  Created by dhomes on 10/17/21.
//

import XCTest
@testable import PenguinPay

class TestTransactionStore: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testTransactionStore() async throws {
        let transaction1 = try await PenguinAPI().sendMoneyTo(Destinatary.mock(), sending: "1010101", receiving: "11111111")
        let transaction2 = try await PenguinAPI().sendMoneyTo(Destinatary.mock(), sending: "1010101", receiving: "11111111")
        let store = TransactionStore()
        store.addTransaction(transaction1)
        store.addTransaction(transaction2)
        XCTAssert(store.transactions.first != nil)
        XCTAssert(store.transactions.first?.id == transaction2.id)
    }

}
