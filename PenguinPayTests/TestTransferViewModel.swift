//
//  TestTransferViewModel.swift
//  PenguinPayTests
//
//  Created by dhomes on 10/17/21.
//

import XCTest
@testable import PenguinPay

class TestTransferViewModel: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testTransferViewModel() async throws {
        let provider = ServedCountryProvider()
        struct MockTestProvider : RateProvider {
            func getRates() async throws -> [Rate] {
                [
                    CurrentRate(code: ServedCountries.kenya.currencyAbbreviation, rate: 1.5),
                    CurrentRate(code: ServedCountries.uganda.currencyAbbreviation, rate: 3),
                    CurrentRate(code: ServedCountries.tanzania.currencyAbbreviation, rate: 10.75),
                    CurrentRate(code: ServedCountries.nigeria.currencyAbbreviation, rate: 100.11010)
                ]
            }
        }
        
        struct MockTransaction : Transaction {
            private(set) var date: Date = Date()
            private(set) var user: User = Destinatary.mock()
            private(set) var id: String = UUID().uuidString
            var sent: String
            var received: String
        }
        
        struct MockAPI : API {
            func sendMoneyTo(_ user: User, sending: String, receiving: String) async throws -> Transaction {
                MockTransaction(sent: sending, received: receiving)
            }
        }
        
        let model = TransferViewModel(countryProvider: provider, rateProvider: MockTestProvider(), api: MockAPI())
        XCTAssertFalse(model.canTransfer) // we havent set a country or a user
        let mockuser = Destinatary.mock()
        model.destinatary = mockuser
        
        // setting the user also sets the country
        XCTAssertNotNil(model.destinatary)
        XCTAssert(model.selectedCountry.id == mockuser.country.id)
        XCTAssertFalse(model.canTransfer) // we havent set yet an amount to transfer
        await model.fetchLatest()
        
        model.sending = "1010101" // USD $85
        XCTAssert(model.canTransfer)
        
        model.selectedCountry = ServedCountries.kenya
        XCTAssert(model.receiving == "1111111") // 127 - We are sending ints & receiving int
        
        model.selectedCountry = ServedCountries.uganda
        XCTAssert(model.receiving == "11111111")
        
        model.selectedCountry = ServedCountries.tanzania
        XCTAssert(model.receiving == "1110010001") // 913
        
        model.selectedCountry = ServedCountries.nigeria
        XCTAssert(model.receiving == "10000100111101") // 8509
        
        
    }
}
