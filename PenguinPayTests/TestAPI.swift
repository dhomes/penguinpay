//
//  TestAPI.swift
//  PenguinPayTests
//
//  Created by dhomes on 10/17/21.
//

import XCTest
@testable import PenguinPay

class TestAPI: XCTestCase {

    override func setUpWithError() throws {
        continueAfterFailure = false
    }

    func testAPI() async throws {
        let transaction = try await PenguinAPI().sendMoneyTo(Destinatary.mock(), sending: "1010101", receiving: "11111111")
        XCTAssert(transaction.user.id == Destinatary.mock().id)
    }
}
